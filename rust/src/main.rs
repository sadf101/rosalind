extern crate bio;
extern crate ordered_float;
extern crate partial_application;
use std::borrow::Borrow;

fn main() {
	println!("{:?}", count_bases(b"ACGT"));
	let testarr = [b'C'; 4];
	let testvec = vec![b'C'; 4];
	println!("{:?}", count_bases(&testarr));
	println!("{:?}", count_bases(&testvec));
	println!("{:?}", std::str::from_utf8(&dna_to_rna(b"ACGT")).unwrap());
	println!("{:?}", std::str::from_utf8(&reverse_complement_sequence(b"ACGT")).unwrap());
	let testrecords = vec![("beans1", b"ACGT"), ("beans2", b"AAGT"), ("beans2", b"ACGG")];
	let beans = fasta_to_max_gc_content(testrecords);
	println!("{:?}", beans);
}

fn count_bases<C: Borrow<u8>, T: IntoIterator<Item = C>>(dna: T) -> (i64, i64, i64, i64) {
	let (mut a, mut c, mut g, mut t) = (0, 0, 0, 0);
	for s in dna {
		match *s.borrow() {
			b'A' => a += 1,
			b'C' => c += 1,
			b'G' => g += 1,
			b'T' => t += 1,
			_ => (),
		}
	}
	(a, c, g, t)
}

fn dna_to_rna<C: Borrow<u8>, T: IntoIterator<Item = C>>(dna: T) -> Vec<u8> {
	dna.into_iter().map(|x| if *x.borrow() != b'T' { *x.borrow() } else {b'U'}).collect()
}

fn complement_base(base: u8) -> u8 {
	match base {
		b'A' => b'T',
		b'C' => b'G',
		b'G' => b'C',
		b'T' => b'A',
		_ => b'_',
	}
}

fn complement_sequence<C: Borrow<u8>, T: IntoIterator<Item = C>>(dna: T) -> Vec<u8> {
	dna.into_iter().map(|x| complement_base(*x.borrow())).collect()
}

fn reverse_complement_sequence<C: Borrow<u8>, T: IntoIterator<Item = C>>(dna: T) -> Vec<u8> {
	complement_sequence(dna).into_iter().rev().collect()
}

fn calc_gc_content<C: Borrow<u8>, T: IntoIterator<Item = C>>(dna: T)  -> f64 {
	let mut total = 0;
	let mut len = 0;
	for s in dna {
		match *s.borrow() {
			b'G' => total += 1,
			b'C' => total += 1,
			_ => ()
		}
		len += 1
	}
	total as f64 / len as f64 * 100.0
}

use bio::io::fasta;
use ordered_float::OrderedFloat;

fn read_fasta_records(filename: &str) -> Vec<(String, Vec<u8>)> {
	let mut out = Vec::new();
	for record in fasta::Reader::from_file(filename).unwrap().records() {
		let record = record.unwrap();
		out.push((
			String::from(record.id()),
			Vec::from(record.seq()),
		));
	}
	out
}

fn fasta_to_max_gc_content<'a, B, D, T>(records: T) -> Option<(&'a str, f64)>
where
	B: Borrow<u8>,
	D: IntoIterator<Item = B>,
	T: IntoIterator<Item = (&'a str, D)>
{
	let mut maxgc = 0.0;
	let mut out = None;
	for record in records {
		let (id, seq) = record;
		let gc = calc_gc_content(seq);
		if gc > maxgc {
			out = Some((id, gc));
			maxgc = gc
		}
	}
	out
}

use std::fmt;

struct Consensus {
	mode: Vec<u8>,
	a: Vec<i64>,
	c: Vec<i64>,
	g: Vec<i64>,
	t: Vec<i64>,
}

impl Consensus {
	fn new(seq_len: usize) -> Self {
		Consensus {
			mode: Vec::with_capacity(seq_len),
			a: vec![0; seq_len],
			c: vec![0; seq_len],
			g: vec![0; seq_len],
			t: vec![0; seq_len],
		}
	}
}

impl fmt::Display for Consensus {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		let mut out: String = String::from(std::str::from_utf8(&self.mode).unwrap());
		out.push('\n');
		let mut az: String = self
			.a
			.iter()
			.map(|x| String::from(" ") + &x.to_string())
			.collect();
		let mut cz: String = self
			.c
			.iter()
			.map(|x| String::from(" ") + &x.to_string())
			.collect();
		let mut gz: String = self
			.g
			.iter()
			.map(|x| String::from(" ") + &x.to_string())
			.collect();
		let mut tz: String = self
			.t
			.iter()
			.map(|x| String::from(" ") + &x.to_string())
			.collect();
		az.push('\n');
		cz.push('\n');
		gz.push('\n');
		tz.push('\n');
		out.push_str("A:");
		out.push_str(&az);
		out.push_str("C:");
		out.push_str(&cz);
		out.push_str("G:");
		out.push_str(&gz);
		out.push_str("T:");
		out.push_str(&tz);
		write!(f, "{}", out)
	}
}

fn consensus<'a, B, D, T>(fasta_data: T) -> Consensus
where
	B: Borrow<u8>,
	D: IntoIterator<Item = B>,
	T: IntoIterator<Item = (&'a str, D)>
{
	let mut seq_len = 0;
	let mut out: Consensus = Consensus::new(seq_len);
	for (i, (_, dna)) in fasta_data.into_iter().enumerate() {
		for (i, c) in dna.into_iter().enumerate() {
			match *c.borrow() {
				b'A' => out.a[i] += 1,
				b'C' => out.c[i] += 1,
				b'G' => out.g[i] += 1,
				b'T' => out.t[i] += 1,
				_ => (),
			}
		}
		seq_len = i;
	}
	for i in 0..seq_len {
		let mut max = -1;
		let mut to_add = b'_';
		if out.a[i] > max {
			to_add = b'A';
			max = out.a[i]
		}
		if out.c[i] > max {
			to_add = b'C';
			max = out.c[i]
		}
		if out.g[i] > max {
			to_add = b'G';
			max = out.g[i]
		}
		if out.t[i] > max {
			to_add = b'T';
		}
		out.mode.push(to_add);
	}
	out
}

fn has_k_overlap(left: &str, right: &str, k: usize) -> bool {
	left[left.len() - k..left.len()] == right[0..k]
}
/*
type Fasta = Vec<(String, String)>;

struct AdjList(Vec<(String, String)>);

impl std::ops::Deref for AdjList {
	type Target = Vec<(String, String)>;

	fn deref(&self) -> &Vec<(String, String)> {
		&self.0
	}
}

impl std::ops::DerefMut for AdjList {
	fn deref_mut(&mut self) -> &mut Vec<(String, String)> {
		&mut self.0
	}
}

impl fmt::Display for AdjList {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		let mut out: String = String::new();
		for (x, y) in self.iter() {
			out.push_str(x);
			out.push_str(" ");
			out.push_str(y);
			out.push('\n');
		}
		write!(f, "{}", out)
	}
}

fn overlap_graph(data: &Fasta, k: usize) -> AdjList {
	let mut out: AdjList = AdjList(Vec::new());
	for (i, (idi, seqi)) in data.iter().enumerate() {
		for (j, (idj, seqj)) in data.iter().enumerate() {
			if i != j && has_k_overlap(seqi, seqj, k) {
				out.push((idi.to_string(), idj.to_string()))
			}
		}
	}
	out
}

#[allow(non_snake_case)]
fn expected_offspring(AAAA: i64, AAAa: i64, AAaa: i64, AaAa: i64, Aaaa: i64, _: i64) -> f64 {
	(AAAA * 2) as f64 + (AAAa * 2) as f64 + (AAaa * 2) as f64 + AaAa as f64 * 1.5 + Aaaa as f64
}

fn shared_motif(data: &Fasta) -> &str {
	let mut out = "";
	let longest = &data
		.iter()
		.max_by(|x, y| x.1.len().cmp(&y.1.len()))
		.unwrap()
		.1;
	for i in 0..longest.len() {
		for j in i + 1..longest.len() {
			let mut is_common = true;
			let cmp_str = &longest[i..j];
			for (_, x) in data.iter() {
				if !x.contains(cmp_str) {
					is_common = false;
					break;
				}
			}
			if is_common && cmp_str.len() > out.len() {
				out = cmp_str;
			}
		}
	}
	out
}

use itertools::Itertools;

struct Permutations(usize);

impl fmt::Display for Permutations {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		let mut out: String = String::new();
		let mut combos = 0;
		for p in (1..self.0 + 1).permutations(self.0) {
			for x in p {
				out.push_str(&x.to_string());
				out.push(' ');
			}
			out.pop();
			out.push('\n');
			combos += 1;
		}
		write!(f, "{}", combos.to_string() + "\n" + &out)
	}
}

fn get_mono_mass(aa: char) -> Option<f64> {
	match aa {
		'A' => Some(71.03711),
		'C' => Some(103.00919),
		'D' => Some(115.02694),
		'E' => Some(129.04259),
		'F' => Some(147.06841),
		'G' => Some(57.02146),
		'H' => Some(137.05891),
		'I' => Some(113.08406),
		'K' => Some(128.09496),
		'L' => Some(113.08406),
		'M' => Some(131.04049),
		'N' => Some(114.04293),
		'P' => Some(97.05276),
		'Q' => Some(128.05858),
		'R' => Some(156.10111),
		'S' => Some(87.03203),
		'T' => Some(101.04768),
		'V' => Some(99.06841),
		'W' => Some(186.07931),
		'Y' => Some(163.06333),
		_ => None,
	}
}

fn get_mono_mass_str(aac: &str) -> Option<f64> {
	let mut out = 0.0;
	for aa in aac.chars() {
		match get_mono_mass(aa) {
			Some(x) => out += x,
			None => return None,
		}
	}
	Some(out)
}

fn restriction_site(dna: &str) -> Vec<(usize, usize)> {
	let mut out = Vec::new();
	for i in 0..dna.len() - 3 {
		let upper = if dna.len() + 1 < i + 13 {
			dna.len() + 1
		} else {
			i + 13
		};
		for j in i + 4..upper {
			println!("{:?}", (i + 1, j - i));
			if dna[i..j] == reverse_complement_sequence(&dna[i..j]) {
				out.push((i + 1, j - i));
			}
		}
	}
	out
}

fn print_sites(sites: &[(usize, usize)]) {
	for (x, y) in sites.iter() {
		println!("{:?} {:?}", x, y);
	}
}

fn is_increasing(xs: &[i64]) -> bool {
	for i in 0..xs.len() - 1 {
		if xs[i] > xs[i + 1] {
			return false;
		}
	}
	true
}

fn is_decreasing(xs: &[i64]) -> bool {
	for i in 0..xs.len() - 1 {
		if xs[i] < xs[i + 1] {
			return false;
		}
	}
	true
} 

/*
TODO: Try making a dynamic programming algo for this.
Something like if for i to j is not increasing or decreasing, then dont bother trying as i -> j + n
cannot then be either increasing or decreasing.
Matrix of booleans that gets filled in as we go along starting from i = 0 then for j onwards.
If we know that 0 -> 1 is not increasing, then 0 -> len is not increasing.
*/
fn longest_subsequence(xs: &[i64]) -> (Vec<&[i64]>, Vec<&[i64]>) {
	let (mut dec, mut inc): (Vec<&[i64]>, Vec<&[i64]>) = (Vec::new(), Vec::new());
	for i in 0..xs.len() {
		for j in i..xs.len() {
			if is_decreasing(&xs[i..j]) {
				if dec.is_empty() || i - j == dec[0].len() {
					dec.push(&xs[i..j]);
				} else {
					if i - j > dec[0].len() {
						dec.clear();
						dec.push(&xs[i..j]);
					}
				}
			} else if is_increasing(&xs[i..j]) {
				if inc.is_empty() || i - j == inc[0].len() {
					inc.push(&xs[i..j]);
				} else {
					if i - j > inc[0].len() {
						inc.clear();
						inc.push(&xs[i..j]);
					}
				}
			}
		}
	}
	(dec, inc)
}

fn dyn_longest_subsequence(xs: &[i64]) -> (Vec<&[i64]>, Vec<&[i64]>) {

use std::fs;
*/

