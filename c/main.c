#include <stdio.h>

struct base_count
{
    int a;
    int c;
    int g;
    int t;
};

struct base_count count_bases(char *dna)
{
    struct base_count out = { .a = 0, .c = 0, .g = 0, .t = 0};
    char *cur = dna;
    while (*cur != '\0') {
        switch (*cur) {
            case 'A': out.a += 1; break;
            case 'C': out.c += 1; break;
            case 'G': out.g += 1; break;
            case 'T': out.t += 1; break;
        }
        cur++;
    }
    return out;
}

char *dna_to_rna(char *dna)
{
    char *cur = dna;
    while (*cur != '\0') {
        if (*cur == 'T') {
            *cur = 'U';
        }
        cur++;
    }
    return dna;
}

char complement_base(char base)
{
    switch (base) {
        case 'A': return 'T';
        case 'C': return 'G';
        case 'G': return 'C';
        case 'T': return 'A';
        default: return '_';
    }
}

int main(int argc, char **argv)
{
    char test_string[] = "ACGT";
    struct base_count bc = count_bases(test_string);
    printf("%d %d %d %d\n", bc.a, bc.c, bc.g, bc.t);
    printf("%s\n", dna_to_rna(test_string));
    return 0;
}