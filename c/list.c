#include <stdlib.h>
#include <stdbool.h>
#include "list.h"

struct array_list new_array_list()
{
    struct array_list out;
    out.size = 0;
    out.allocated = 0;
    out.array = NULL;
    return out;
}

void delete_array_list(struct array_list l)
{
    free(l.array);
}

bool push(struct array_list *l, char c)
{
    /*
    if (l->size == 0) {
        l->array = malloc(2 * sizeof(char));
        if (l->array == NULL) {
            return false;
        }
        l->array[0] = c;
        l->allocated = 2;
        l->size = 1;
    } else if (l->size == l->allocated) {
        char *new = realloc(l->array, l->allocated * 2 * sizeof(char));
        if (new == NULL) {
            return false;
        }
        l->allocated *= 2;
        l->array = new;
        l->array[l->size] = c;
        l->size++;
    } else {
        l->array[l->size] = c;
        l->size++;
    }
    return true;
    */
   return insert(l, l->size, c);
}

static void shuffle_up(struct array_list *l, size_t from) {
    for (int cur = l->size - 1; cur >= from; cur--) {
        l->array[cur + 1] = l->array[cur];
    }
}

char pop(struct array_list *l)
{
    l->size--;
    return l->array[l->size];
}

char get(struct array_list l, size_t i)
{
    return l.array[i];
}

void rem(struct array_list *list, size_t i)
{
    struct array_list l = *list;
    while (i < l.size - 1) {
        l.array[i] = l.array[i++];
    }
    l.size--;
}

bool insert(struct array_list *l, size_t i, char c)
{
    if (i > l->size) {
        return false;
    }
    if (l->size == 0) {
        l->array = malloc(2 * sizeof(char));
        if (l->array == NULL) {
            return false;
        }
        l->array[0] = c;
        l->allocated = 2;
        l->size = 1;
    } else if (l->size == l->allocated) {
        char *new = realloc(l->array, (l->allocated *= 2) * sizeof(char));
        if (new == NULL) {
            return false;
        }
        l->array = new;
        shuffle_up(l, i);
        l->size++;
        l->array[i] = c;
    } else {
        shuffle_up(l, i);
        l->size++;
        l->array[i] = c;
    }
    return true;
}

#include <stdio.h>

int main(int argc, char const *argv[])
{
    struct array_list l = new_array_list();
    for (int i = 0; i <= 10; i++) {
        insert(&l, 0, (char)i);
    }
    printf("here now\n");
    printf("size %d\n", l.size);
    int count = 0;
    for (int i = 0; i <= 10; i++) {
        int beans = pop(&l);
        printf("here now %d\n", beans);
        count += beans;
    }
    delete_array_list(l);
    
    return count;
}
