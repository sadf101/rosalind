struct array_list
{
    size_t size;
    size_t allocated;
    char *array;
};

bool insert(struct array_list *l, size_t i, char c);